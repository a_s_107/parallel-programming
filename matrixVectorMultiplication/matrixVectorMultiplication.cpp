﻿#include <iostream>
#include "mpi.h"

long* mulMatrixVector(int n, long**matrix, long*vector) {
    int rank;
    int size;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0 && (!matrix || !vector))
        return nullptr;

    MPI_Bcast(&vector[0], n, MPI_LONG, 0, MPI_COMM_WORLD);
    
    int partSize = n / size;
    if (rank < n - partSize * size)
        partSize++;

    long* partMatrix = new long[partSize * n];
    
    if (rank == 0) {
        long* matrixConvertedToArray = new long[n * n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrixConvertedToArray[i * n + j] = matrix[i][j];
            }
        }

        int* sendcounts = new int[size];
        int* displsSend = new int[size];

        int sum = 0;
        for (int i = 0; i < size; i++) {
            int count = n / size;

            if (i < n - count * size) {
                count++;
            }

            sendcounts[i] = count * n;
            displsSend[i] = sum;
            sum += sendcounts[i];
        }

        MPI_Scatterv(&matrixConvertedToArray[0], sendcounts, displsSend, MPI_LONG, &partMatrix[0], partSize * n, MPI_LONG, 0, MPI_COMM_WORLD);

        delete[] matrixConvertedToArray;
        delete[] sendcounts;
        delete[] displsSend;
    }
    else {
        MPI_Scatterv(nullptr, nullptr, nullptr, MPI_LONG, &partMatrix[0], partSize * n, MPI_LONG, 0, MPI_COMM_WORLD);
    }

    long* valuesInVector = new long[partSize];

    for (int i = 0; i < partSize; i++) {
        valuesInVector[i] = 0;
    }

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < partSize; j++) {
            valuesInVector[j] += partMatrix[i + j * n] * vector[i];
        }
    }

    delete[] partMatrix;

    if (rank == 0) {
        long* resultVector = new long[n];

        int* recvcounts = new int[size];
        int* displsRecv = new int[size];

        int sum = 0;
        for (int i = 0; i < size; i++) {
            int count = n / size;

            if (i < n - count * size) {
                count++;
            }

            recvcounts[i] = count;
            displsRecv[i] = sum;
            sum += recvcounts[i];
        }

        MPI_Gatherv(&valuesInVector[0], partSize, MPI_LONG, &resultVector[0], recvcounts, displsRecv, MPI_LONG, 0, MPI_COMM_WORLD);

        delete[] valuesInVector;
        delete[] recvcounts;
        delete[] displsRecv;

        return resultVector;
    }
    else {
        MPI_Gatherv(&valuesInVector[0], partSize, MPI_LONG, nullptr, nullptr, nullptr, MPI_LONG, 0, MPI_COMM_WORLD);

        delete[] valuesInVector;

        return nullptr;
    }
}

int main(int argc, char** argv)
{
    int rank;
    int size;
    int n;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0) {
        std::cout << "Enter length of vector: ";
        std::cin >> n;

        if (n < size) {
            std::cout << "The value must be greater than the number of threads";

            return 0;
        }

        double timeWork = MPI_Wtime();

        MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

        long** matrix = new long* [n];
        for (int i = 0; i < n; i++)
            matrix[i] = new long[n];
        long* vector = new long[n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrix[i][j] = i;
            }
            vector[i] = 1;
        }

        long* resultVector = mulMatrixVector(n, matrix, vector);

        timeWork = MPI_Wtime() - timeWork;

        if (n < 21) {
            std::cout << "Matrix: (\n";
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    std::cout << " " << matrix[i][j];
                }
                std::cout << std::endl;
            }
            std::cout << " )" << std::endl;
            std::cout << "Vector: (";
            for (int i = 0; i < n; i++) {
                std::cout << " " << vector[i];
            }
            std::cout << " )" << std::endl;
        }

        std::cout << "Result: (";
        for (int i = 0; i < n; i++) {
            std::cout << " " << resultVector[i];
        }
        std::cout << " )" << std::endl;
        std::cout << "Time: " << timeWork << std::endl;

        for (int i = 0; i < n; i++)
            delete[] matrix[i];

        delete[] matrix;
        delete[] vector;
    }
    else {
        MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
        
        long* vector = new long[n];
        mulMatrixVector(n, nullptr, vector);
        
        delete[] vector;
    }

    MPI_Finalize();
    return 0;
}
