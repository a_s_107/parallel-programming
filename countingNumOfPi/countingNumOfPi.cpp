﻿#include <iostream>
#include "mpi.h"

double countingNumOfPi(int numberOfSimbolsAfterComma) {
    int rank;
    int size;
    double partPi = 0;
    double e = 1.0 / pow(10, numberOfSimbolsAfterComma + 1);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    double memberOfNumber = 0;
    int i = rank;
    while (abs(memberOfNumber = pow(-1, i) / (2.0 * i + 1)) >= e) {
        partPi += memberOfNumber;
        i += size;
    }

    double result = 0;
    MPI_Reduce(&partPi, &result, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    
    return result;
}

int main(int argc, char** argv)
{
    int rank;
    int numberOfSimbolsAfterComma;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (rank == 0) {
        std::cout << "Enter the number of decimal places: ";
        std::cin >> numberOfSimbolsAfterComma;

        double timeWork = MPI_Wtime();

        MPI_Bcast(&numberOfSimbolsAfterComma, 1, MPI_INT, 0, MPI_COMM_WORLD);

        double pi = countingNumOfPi(numberOfSimbolsAfterComma);

        timeWork = MPI_Wtime() - timeWork;

        std::cout << "Time: " << timeWork << std::endl;
        std::cout.precision(numberOfSimbolsAfterComma + 1);
        std::cout << "Pi: " << pi * 4 << std::endl;
    }
    else {
        MPI_Bcast(&numberOfSimbolsAfterComma, 1, MPI_INT, 0, MPI_COMM_WORLD);
        countingNumOfPi(numberOfSimbolsAfterComma);
    }

    MPI_Finalize();
    return 0;
}
