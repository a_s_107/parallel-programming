﻿#include <iostream>
#include "mpi.h"

long** mulMatrixMatrix(int n, long** matrixA, long** matrixB) {
    int rank;
    int size;
    MPI_Status status;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0 && (!matrixA || !matrixB))
        return nullptr;

    int partSize = n / size;
    int maxPartSize = (n % size != 0) ? partSize + 1 : partSize;
    long* arrB = new long[maxPartSize * n];

    if (rank < n - partSize * size)
        partSize++;

    long* arrA = new long[partSize * n];

    if (rank == 0) {
        int sum = 0;

        for (int k = 0; k < size; k++) {
            int count = n / size;

            if (k < n - count * size) {
                count++;
            }

            int sendcount = count * n;
            int displ = sum;
            sum += sendcount;

            long* a = new long[sendcount];
            long* b = new long[sendcount];

            for (int i = displ / n; i < sum / n; i++) {
                for (int j = 0; j < n; j++) {
                    a[(i - displ / n) * n + j] = matrixA[i][j];
                    b[(i - displ / n) * n + j] = matrixB[j][i];
                }
            }

            if (k == 0) {
                for (int i = 0; i < sendcount; i++) {
                    arrA[i] = a[i];
                    arrB[i] = b[i];
                }
            }
            else {
                MPI_Send(&a[0], sendcount, MPI_LONG, k, 99, MPI_COMM_WORLD);
                MPI_Send(&b[0], sendcount, MPI_LONG, k, 99, MPI_COMM_WORLD);
            }

            delete[] a;
            delete[] b;
        }
    }
    else {
        MPI_Recv(&arrA[0], partSize * n, MPI_LONG, 0, 99, MPI_COMM_WORLD, &status);
        MPI_Recv(&arrB[0], partSize * n, MPI_LONG, 0, 99, MPI_COMM_WORLD, &status);
    }

    long* partResultMatrix = new long[partSize * n];

    for (int i = 0; i < partSize * n; i++) {
        partResultMatrix[i] = 0;
    }

    int* startOfDataRanks = new int[size];

    for (int i = 0; i < size; i++) {
        if (i == 0) {
            startOfDataRanks[i] = 0;
            continue;
        }

        startOfDataRanks[i] = n / size;

        if (i <= n - startOfDataRanks[i] * size) {
            startOfDataRanks[i]++;
        }

        startOfDataRanks[i] += startOfDataRanks[i - 1];
    }


    for (int i = 0; i < n; i++) {
        for (int j = 0; j < partSize; j++) {
            for (int k = 0; k < partSize; k++) {
                partResultMatrix[j * n + startOfDataRanks[rank] + k] += arrA[i + j * n] * arrB[i + k * n];
            }
        }
    }

    MPI_Request reqSend;

    long* arrBadditional = new long[maxPartSize * n];

    int newPartSize = partSize;
    for (int a = 1; a < size; a++) {
        int preRank = (rank - a + size) % size;
        int newRank = (rank + 1) % size;

        if (a % 2 == 0)
            MPI_Isend(&arrBadditional[0], newPartSize * n, MPI_LONG, newRank, 99, MPI_COMM_WORLD, &reqSend);
        else
            MPI_Isend(&arrB[0], newPartSize * n, MPI_LONG, newRank, 99, MPI_COMM_WORLD, &reqSend);
        
        if (preRank < size - 1) {
            newPartSize = startOfDataRanks[preRank + 1] - startOfDataRanks[preRank];
        }
        else {
            newPartSize = n - startOfDataRanks[preRank];
        }

        if (a % 2 == 0)
            MPI_Recv(&arrB[0], newPartSize * n, MPI_LONG, (rank - 1 + size) % size, 99, MPI_COMM_WORLD, &status);
        else
            MPI_Recv(&arrBadditional[0], newPartSize * n, MPI_LONG, (rank - 1 + size) % size, 99, MPI_COMM_WORLD, &status);

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < partSize; j++) {
                for (int k = 0; k < newPartSize; k++) {
                    if (a % 2 == 0)
                        partResultMatrix[j * n + startOfDataRanks[(rank - a + size) % size] + k] += arrA[i + j * n] * arrB[i + k * n];
                    else
                        partResultMatrix[j * n + startOfDataRanks[(rank - a + size) % size] + k] += arrA[i + j * n] * arrBadditional[i + k * n];
                }
            }
        }

        MPI_Wait(&reqSend, &status);
    }

    delete[] startOfDataRanks;
    delete[] arrBadditional;

    if (rank == 0) {
        long** result = new long* [n];
        for (int i = 0; i < n; i++) {
            result[i] = new long[n];
        }

        int sum = 0;

        for (int k = 0; k < size; k++) {
            int count = n / size;

            if (k < n - count * size) {
                count++;
            }

            int sendcount = count * n;
            int displ = sum;
            sum += sendcount;

            long* part = new long[sendcount];

            if (k == 0) {
                for (int i = 0; i < sendcount; i++) {
                    part[i] = partResultMatrix[i];
                }
            }
            else {
                MPI_Recv(&part[0], sendcount, MPI_LONG, k, 99, MPI_COMM_WORLD, &status);
            }

            for (int i = displ / n; i < sum / n; i++) {
                for (int j = 0; j < n; j++) {
                    result[i][j] = part[(i - displ / n) * n + j];
                }
            }

            delete[] part;
        }

        delete[] partResultMatrix;

        return result;
    }
    else {
        MPI_Send(&partResultMatrix[0], partSize* n, MPI_LONG, 0, 99, MPI_COMM_WORLD);

        delete[] partResultMatrix;

        return nullptr;
    }
}

int main(int argc, char** argv)
{
    int rank;
    int size;
    int n;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0) {
        std::cout << "Enter matrix size: ";
        std::cin >> n;

        if (n < size) {
            std::cout << "The value must be greater than the number of threads";

            return 0;
        }

        double timeWork = MPI_Wtime();

        MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

        long** matrixA = new long* [n];
        long** matrixB = new long* [n];
        for (int i = 0; i < n; i++) {
            matrixA[i] = new long[n];
            matrixB[i] = new long[n];
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                matrixA[i][j] = i;
                matrixB[i][j] = j;
            }
        }

        long** resultMatrix = mulMatrixMatrix(n, matrixA, matrixB);

        timeWork = MPI_Wtime() - timeWork;

        if (n < 21) {
            std::cout << "MatrixA: (\n";
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    std::cout << " " << matrixA[i][j];
                }
                std::cout << std::endl;
            }
            std::cout << " )" << std::endl;
            std::cout << "MatrixB: (\n";
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    std::cout << " " << matrixB[i][j];
                }
                std::cout << std::endl;
            }
            std::cout << " )" << std::endl;
            std::cout << "Result: (\n";
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    std::cout << " " << resultMatrix[i][j];
                }
                std::cout << std::endl;
            }
            std::cout << " )" << std::endl;
        }

        std::cout << "Time: " << timeWork << std::endl;

        for (int i = 0; i < n; i++) {
            delete[] matrixA[i];
            delete[] matrixB[i];
        }

        delete[] matrixA;
        delete[] matrixB;
    }
    else {
        MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
        mulMatrixMatrix(n, nullptr, nullptr);
    }

    MPI_Finalize();
    return 0;
}
