﻿#include <iostream>
#include "mpi.h"

int sum(int* arr, int n) {
    int result = 0;

    for (int i = 0; i < n; i++) {
        result += arr[i];
    }

    return result;
}

int sumOfNumbers(int* arr, int n) {
    int rank;
    int size;

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int sizePartArr = n / size;
    if (rank < n - sizePartArr * size)
        sizePartArr++;

    int* partArr = new int[sizePartArr];

    if (rank == 0) {
        int* sendcounts = new int[size];
        int* displs = new int[size];

        int sum = 0;
        for (int i = 0; i < size; i++) {
            int count = n / size;

            if (i < n - count * size) {
                count++;
            }

            sendcounts[i] = count;
            displs[i] = sum;
            sum += sendcounts[i];
        }

        MPI_Scatterv(&arr[0], sendcounts, displs, MPI_INT, &partArr[0], sizePartArr, MPI_INT, 0, MPI_COMM_WORLD);
    }
    else {
        MPI_Scatterv(NULL, NULL, NULL, MPI_INT, &partArr[0], sizePartArr, MPI_INT, 0, MPI_COMM_WORLD);
    }

    int sumProcess = sum(partArr, sizePartArr);

    delete[] partArr;

    MPI_Status status;

    int i = 2;
    while (i < size * 2) {
        if (rank % i == 0 && rank + i / 2 < size) {
            int sumRecv;
            MPI_Recv(&sumRecv, 1, MPI_INT, rank + i / 2, 99, MPI_COMM_WORLD, &status);
            sumProcess += sumRecv;
        }
        else if (rank % i == i / 2) {
            MPI_Send(&sumProcess, 1, MPI_INT, rank - i / 2, 99, MPI_COMM_WORLD);
            break;
        }
        i *= 2;
    }

    return sumProcess;
}

int main(int argc, char **argv) {
    int rank;
    int size;
    int upperBoundOfSum;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (rank == 0) {
        std::cout << "Enter upper bound of summation: ";
        std::cin >> upperBoundOfSum;
        
        if (upperBoundOfSum < size) {
            std::cout << "The value must be greater than the number of threads";
            
            return 0;
        }

        double timeWork = MPI_Wtime();

        MPI_Bcast(&upperBoundOfSum, 1, MPI_INT, 0, MPI_COMM_WORLD);

        int* arr = new int[upperBoundOfSum];

        if (rank == 0) {
            for (int i = 0; i < upperBoundOfSum; i++) {
                arr[i] = 1;
            }
        }

        int resultSum = sumOfNumbers(arr, upperBoundOfSum);

        timeWork = MPI_Wtime() - timeWork;

        std::cout << "Sum: " << resultSum << std::endl;
        std::cout << "Time: " << timeWork << std::endl;
    }
    else {
        MPI_Bcast(&upperBoundOfSum, 1, MPI_INT, 0, MPI_COMM_WORLD);
        sumOfNumbers(nullptr, upperBoundOfSum);
    }

    MPI_Finalize();
    return 0;
}
